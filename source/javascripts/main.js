//= require_tree ./lib/

(function($) {

$(function() {

	var
		css = {
			active: '_active',
			prev: '_prev',
			dur: 300
		},

		$slider, slider;

	slider = new Flow('.slider li');

	$slider = $('.slider');

	slider.movement = function(position) {
		var $items = $(this.slides), $item = $items.eq(position), $prev = $items.filter('.'+css.active);

		$item.addClass(css.active).css({opacity: 0}).animate({opacity: 1}, css.dur);
		$prev.addClass(css.prev).removeClass(css.active);

		setTimeout(function() {
			$prev.removeClass(css.prev);
		}, css.dur);
	};

	$slider.find('.slider-controls')
		.on('click', 'span', function() {
			var $this = $(this), index = $this.index();

			slider.show(index);
			$this.addClass(css.active).siblings().removeClass(css.active);
		});

});

})(jQuery);
